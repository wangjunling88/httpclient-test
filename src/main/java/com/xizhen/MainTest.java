package com.xizhen;
import com.xizhen.thread.MyThread;
import org.apache.log4j.Logger;


/**
 * 并行且异步发送n个请求主程序入口
 */
public class MainTest {
    private static Logger logger = Logger.getLogger(MainTest.class);

    /**
     * 发送并发异步请求
     */

    public static void main(String[] argv) {
        /**
         * 多线程并行发送请求
         * 可以把i<3改成条件i<n这样会并行且异步发送n个请求
         */
        for (int i = 0; i < 3; i++) {
            long timeout = (i + 1) * 1000;
            Thread t = new MyThread(timeout);
            logger.info("Thread name is" + t.getName() + " this thread set timeout is：" + timeout + "ms");
            t.start();
        }

    }


}




