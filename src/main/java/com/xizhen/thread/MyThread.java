package com.xizhen.thread;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;

/**
 * Java实现多线程的方式
 * 继承Thread类，重写run方法
 */
public class MyThread extends Thread {
    private static Logger logger = Logger.getLogger(MyThread.class);

    //超时时长定义
    private long timeout;

    public MyThread(Long timeout) {
        this.timeout = timeout;
    }

    @Override
    public void run() {
        //此处为thread执行的任务内容
        logger.info(" caller thread name is :" + Thread.currentThread().getName());
        CloseableHttpAsyncClient httpclient = HttpAsyncClients.createDefault();
        httpclient.start();
        final CountDownLatch latch = new CountDownLatch(1);
        final HttpPost request = new HttpPost("http://localhost:8080/selectProductById");
        logger.info(" caller thread id is :" + Thread.currentThread().getId());
        httpclient.execute(request, new FutureCallback<HttpResponse>() {
            long start = System.currentTimeMillis();
            public void completed(final HttpResponse response) {
                latch.countDown();
                long end = System.currentTimeMillis();
                long cosumertime = end - start;
                logger.info("set timeout is:" + timeout+"ms");
                //如果超时了就返回错误
                if (cosumertime > timeout) {
                    logger.error("this  thread is timeout,name is:" + Thread.currentThread().getName());
                }
                logger.info("response time is:" + cosumertime+"ms");
                logger.info(" callback thread id is : " + Thread.currentThread().getId());
                logger.info(request.getRequestLine() + "->" + response.getStatusLine());
                try {
                    String content = EntityUtils.toString(response.getEntity(), "UTF-8");
                    logger.info(" response content is : " + content);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            public void failed(final Exception ex) {
                latch.countDown();
                logger.info(request.getRequestLine() + "->" + ex);
                logger.info(" callback thread id is : " + Thread.currentThread().getId());
            }

            public void cancelled() {
                latch.countDown();
                logger.info(request.getRequestLine() + " cancelled");
                logger.info(" callback thread id is : " + Thread.currentThread().getId());
            }

        });
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            httpclient.close();
        } catch (IOException ignore) {

        }
    }
}